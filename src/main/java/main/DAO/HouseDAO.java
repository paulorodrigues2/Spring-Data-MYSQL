package main.java.main.DAO;

import java.util.Collection;

import main.java.main.Entitys.House;
import main.java.main.Entitys.Person;

//Acess methods of House object Pojo
public interface HouseDAO {
	Collection<House> getAllHouses();

	Person getHouseByName(String name);

	void createHouse(House house);

	void updateHouse(House house);

	void deleteHouse(int HouseId);

	boolean PersonExists(String name, String sobrename);
}
