package main.java.main.ServiceSpring;

import java.util.Collection;

import main.java.main.Entitys.House;

public interface IHouseService {
	Collection<House> getAllHouses();

	House getHousebyName(String HouseName);

	boolean createHouse(House House);

	void updateHouse(House House);

	void deleteHouse(int HouseId);
}