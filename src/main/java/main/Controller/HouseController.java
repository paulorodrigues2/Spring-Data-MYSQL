package main.java.main.Controller;

import java.util.Collection;

import main.java.main.Entitys.House;
import main.java.main.ServiceSpring.HouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HouseController {

	@Autowired
	private HouseService houseService;

	@RequestMapping(value = "/houses/get-all", method = RequestMethod.GET)
	public Collection<House> getAllHouses() {
		return houseService.getAllHouses();
	}
}
